<?php

namespace tstech\controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use tstech\models\DepositsHistories;

/**
 * Class MerchantController
 * @package MasterPass\controllers
 * @property \Psr\Log\LoggerInterface $logger
 * @property \Illuminate\Database\Capsule\Manager $db
 * @property \Psr\Container\ContainerInterface container
 */
class DepositsHistoriesController implements ControllerInterface
{

    public function __construct($container)
    {
        $this->container = $container;
        $this->db;

    }


    public function __get($name)
    {
        return $this->container->get($name);
    }

    public function listRecords(Request $request, Response $response, $args)
    {
        return $response->getBody()->write(DepositsHistories::all()->toJson());

    }

    public function saveRecord(Request $request, Response $response, $args)
    {
        $payload = $request->getParsedBody();

        $record = new DepositsHistories();

        if (isset($payload['id'])) {
            $res = DepositsHistories::find($payload['id']);
            $this->logger->debug(print_r('$deposit', 1));
            $this->logger->debug(print_r($res, 1));
            if ($res) {
                $record = $res;
            }
        }
        foreach ($payload as $key => $val) {
            $record->$key = $val;
        }

        $record->save();
        return $response->getBody()->write($record->toJson());
    }

}