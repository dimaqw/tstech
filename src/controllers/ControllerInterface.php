<?php

namespace tstech\controllers;

use Slim\Http\Request;
use Slim\Http\Response;


interface ControllerInterface
{

    public function listRecords(Request $request, Response $response, $args);

    public function saveRecord(Request $request, Response $response, $args);

}