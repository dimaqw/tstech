<?php

namespace tstech\controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use tstech\models\DepositsTypes;

/**
 * Class MerchantController
 * @package MasterPass\controllers
 * @property \Psr\Log\LoggerInterface $logger
 * @property \Illuminate\Database\Capsule\Manager $db
 * @property \Psr\Container\ContainerInterface container
 */
class DepositsTypesController implements ControllerInterface
{

    public function __construct($container)
    {
        $this->container = $container;
    }


    public function __get($name)
    {
        return $this->container->get($name);
    }

    public function listRecords(Request $request, Response $response, $args)
    {
        return $response->withJson(DepositsTypes::all());
    }

    public function saveRecord(Request $request, Response $response, $args)
    {
        $payload = $request->getParsedBody();

        $record = new DepositsTypes();

        if (isset($payload['id'])) {
            $res = DepositsTypes::find($payload['id']);
            if ($res) {
                $record = $res;
            }
        }
        foreach ($payload as $key => $val) {
            $record->$key = $val;
        }

        $record->save();
        return $response->getBody()->write($record->toJson());
    }


}