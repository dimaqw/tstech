<?php

namespace tstech\controllers;

use Slim\Http\Request;
use Slim\Http\Response;
//use tstech\controllers\ControllerInterface;
use tstech\models\Contacts;
use tstech\models\Deposits;

/**
 * Class MerchantController
 * @package MasterPass\controllers
 * @property \Psr\Log\LoggerInterface $logger
 * @property \Illuminate\Database\Capsule\Manager $db
 * @property \Psr\Container\ContainerInterface container
 */
class ContactsController implements ControllerInterface
{

    public function __construct($container)
    {
        $this->container = $container;
    }


    public function __get($name)
    {
        return $this->container->get($name);
    }

    public function listRecords(Request $request, Response $response, $args)
    {
        return $response->withJson(Contacts::all());
    }

    public function saveRecord(Request $request, Response $response, $args)
    {
        $payload = $request->getParsedBody();

        $record = new Contacts();

        if (isset($payload['id'])) {
            $res = Contacts::find($payload['id']);
            if ($res) {
                $record = $res;
            }
        }
        foreach ($payload as $key => $val) {
            $record->$key = $val;
        }

        $record->save();
        return $response->withJson($record);
    }

    public function listDeposits(Request $request, Response $response)
    {
        $this->logger->debug(__METHOD__);

        $contact_id = $request->getParsedBodyParam('contact_id','');
        $histories = $request->getParsedBodyParam('histories',false);

        if($histories){
            $deposits = Contacts::with('deposits.histories')->find($contact_id);

        }else{
            $deposits = Contacts::with('deposits')->find($contact_id);
        }


        if(!$contact_id){
            $deposits = Contacts::with('deposits')->get();
        }

        return $response->withJson($deposits);

    }


}