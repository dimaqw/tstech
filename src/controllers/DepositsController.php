<?php

namespace tstech\controllers;

use Illuminate\Database\Eloquent\Builder;
use Slim\Http\Request;
use Slim\Http\Response;
//use tstech\controllers\ControllerInterface;
use tstech\models\Deposits;
use tstech\models\DepositsHistories;
use tstech\models\DepositsTypes;

//use Illuminate\Support\Facades\DB;
use Illuminate\Database\Capsule\Manager as DB;


/**
 * Class MerchantController
 * @package MasterPass\controllers
 * @property \Psr\Log\LoggerInterface $logger
 * @property \Illuminate\Database\Capsule\Manager $db
 * @property \Psr\Container\ContainerInterface container
 */
class DepositsController implements ControllerInterface
{

    public function __construct($container)
    {
        $this->container = $container;
        $this->db;

    }


    public function __get($name)
    {
        return $this->container->get($name);
    }

    public function listRecords(Request $request, Response $response, $args)
    {
        return $response->withJson(Deposits::all());

    }

    public function saveRecord(Request $request, Response $response, $args)
    {
        $this->logger->debug(__METHOD__);

        $payload = $request->getParsedBody();

        $record = new Deposits();

        if (isset($payload['id'])) {
            $res = Deposits::find($payload['id']);
            if ($res) {
                $record = $res;
            }
        }
        foreach ($payload as $key => $val) {
            $record->$key = $val;
        }

        $record->save();
        return $response->withJson($record);
    }



    public function listHistory(Request $request, Response $response)
    {
        $this->logger->debug(__METHOD__);

        $deposit_id = $request->getParsedBodyParam('deposit_id','');

        if(!$deposit_id){
            return $response->withJson(['msg' => 'deposit_id not passed',]);
        }


        $deposits = Deposits::with('histories')->find($deposit_id);

        if(!$deposit_id){
            $deposits = Deposits::with('histories')->get();
        }
        return $response->withJson($deposits);


    }



    public function processDebit(Request $request, Response $response, $args)
    {
        /** @var DepositsTypes[] $deposits_types */
        $deposits_types = DepositsTypes::where('status', '=' , 'active')->get();
        $deposits = [];
        foreach ($deposits_types as $deposit_type) {
            $deposits = $this->debitDeposits($deposit_type);

            foreach ($deposits as &$deposit) {
                $this->createDebitHistory($deposit, $deposit_type);
            }
        }
        return $response->withJson($deposits_types);

    }

    public function processAccrual(Request $request, Response $response, $args)
    {
        //если сегодня 2017-02-28 то это последний день и значит 29,30,31 не будет, значит нужно начислить проценты и на эти даты.
        //$current_date = date_create('2017-02-28');
        $current_date = date_create();

        //количество дней в текущем месяце
        $days =  cal_days_in_month ( CAL_GREGORIAN , $current_date->format('n'), $current_date->format('y'));
        //если сегодняшний день последний в месяце
        if((int)$current_date->format('d') === $days){
            /** @var Builder $query */
            $query = Deposits::whereRaw('DAY(`created_at`) >= ' . $current_date->format('d'));
        }else{
            $query = Deposits::whereRaw('DAY(`created_at`) = ' . $current_date->format('d'));
        }

        $deposits = $query->get();

        foreach ($deposits as &$deposit) {
            $deposits = $this->createAccrualHistory($deposit);
        }
        return $response->withJson($deposits);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @desc передать id DepositsTypes, вернет все депозиты попадающие под эту категорию
     * @return Response
     */
    public function showDebitDeposits(Request $request, Response $response, $args)
    {
        $payload = $request->getParsedBody();

        if (isset($payload['id'])) {
            $res = DepositsTypes::find($payload['id']);
            if ($res) {
                $deposit_type = $res;
                $deposits = $this->debitDeposits($deposit_type);
                return $response->withJson($deposits);
            }
        }
        return $response->withJson(['msg' => 'id not found',]);
    }

    private function createAccrualHistory(Deposits $deposit)
    {
        $this->logger->debug(__METHOD__);

        $history = new DepositsHistories();
        $history->incoming_balance = $deposit->balance ;
        $amount = (float) ($deposit->balance * ($deposit->percent / 100));
        $history->amount = $amount;
        $history->outgoing_balance = $deposit->balance + $history->amount;
        $history->deposit_id = $deposit->id;
        $history->type = DepositsHistories::TYPE_INCREASE; //начисление

        //************ THIS TRANSACTION OPERATION AND ROLLBACK **********
        //todo сделать в sql как транзакцию
        $history->save();
        $deposit->balance = $history->outgoing_balance;
        $deposit->save();

        //************ THIS TRANSACTION OPERATION AND ROLLBACK **********

        $this->logger->debug(print_r($history->toArray(),1));
        return ($history);

    }


    /**
     * @param DepositsTypes $deposit_type
     * @return Deposits[]
     */
    private function debitDeposits(DepositsTypes $deposit_type)
    {
        $this->logger->debug(__METHOD__);
        $this->logger->debug(print_r($deposit_type->toArray(),1));
        /** @var Builder $query */
        $query = Deposits::whereRaw('1=1');

        if(null !== $deposit_type->lbound){
            $query->where('balance', '>=' , $deposit_type->lbound);
        }
        if(null !== $deposit_type->ubound){
            $query->where('balance', '<' , $deposit_type->ubound);
        }
        $this->logger->debug(print_r($query->toSql(),1));

        /** @var Deposits[] $deposits */
        $deposits = $query->get();

        $this->logger->debug(print_r($deposits->toArray(),1));
        return ($deposits);

    }

    private function createDebitHistory(Deposits $deposit, DepositsTypes $deposit_type)
    {
        $this->logger->debug(__METHOD__);

        $history = new DepositsHistories();
        $history->incoming_balance = $deposit->balance ;
        $amount = $deposit->balance * ($deposit_type->percent / 100);
        //если баланс получился меньше минимального то устанавливаем минимальный
        if(!empty($deposit_type->lamount) && $amount < $deposit_type->amount){
            $amount = $deposit_type->amount;
        }
        //если баланс получился больше максимального то устанавливаем максимальный
        if(!empty($deposit_type->uamount) && $amount > $deposit_type->amount){
            $amount = $deposit_type->uamount;
        }
        $coefficient =  $this->calculatePartCoefficient((string) $deposit->created_at);
        $history->amount = $amount * $coefficient;
        $history->outgoing_balance = $deposit->balance - $history->amount;
        $history->deposit_id = $deposit->id;
        $history->type = DepositsHistories::TYPE_DECREASE; //списание

        //************ THIS TRANSACTION OPERATION AND ROLLBACK **********
        //todo сделать в sql как транзакцию
        $history->save();
        $deposit->balance = $history->outgoing_balance;
        $deposit->save();

        //************ THIS TRANSACTION OPERATION AND ROLLBACK **********

        $this->logger->debug(print_r($history->toArray(),1));
        return ($history);

    }



    /**
     * @param string $date
     * @return float
     */
    private function calculatePartCoefficient (string $date)
    {
        $this->logger->debug(__METHOD__);

        $datetime = date_create($date);
        $current_date = date_create();
        $diff = ((int) ($current_date->format('Y') - $datetime->format('Y')) * 12) + (int) ($current_date->format('n') - $datetime->format('n'));
        if($diff === 1){
            $days =  cal_days_in_month ( CAL_GREGORIAN , $datetime->format('n'), $datetime->format('y'));
            $days_diff = $days - (int) $datetime->format('d') + 1;
            return round($days_diff / $days,2);
        }
        return 1;
    }


    public function reportDaily (Request $request, Response $response, $args)
    {
        $this->logger->debug(__METHOD__);

        $query = DB::select("
        SELECT
  ifnull(debit,0) debit,
  ifnull(accrual,0) accrual,
  debit_date,
  accrual_date,
  ifnull(accrual,0) - ifnull(debit,0) diffrence
FROM (
       SELECT
         ifnull(SUM(amount),0)      debit,
         DATE(created_at) debit_date
       FROM deposits_histories
       WHERE type = 'debit'
       GROUP BY  debit_date
     ) AS debit

  LEFT JOIN (

               SELECT
                 accrual,
                 accrual_date
               FROM (
                      SELECT
                        SUM(amount)      accrual,
                        DATE(created_at) accrual_date
                      FROM deposits_histories
                      WHERE type = 'accrual'
                      GROUP BY accrual_date
                    ) AS debit
             ) AS accrual ON debit.debit_date = accrual.accrual_date;
        ");


        return $response->withJson($query);
    }
}