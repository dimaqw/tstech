<?php

namespace tstech\controllers;

//use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use tstech\controllers\ControllerInterface;

/**
 * Class MerchantController
 * @package MasterPass\controllers
 * @property \Psr\Log\LoggerInterface $logger
 * @property \Illuminate\Database\Capsule\Manager $db
 * @property \Psr\Container\ContainerInterface container
 */
class ModelController
{
    protected $container;
    const AVAILABLE_MODELS = [
        'deposits' => DepositsController::class,
        'Contacts' => ContactsController::class,
        'Default' => ContactsController::class,
        'deposits_histories' => DepositsHistoriesController::class,
        'deposits_types' => DepositsTypesController::class,
    ];

    public function __construct($container)
    {
        $this->container = $container;
        $this->db;
    }


    public function __get($name)
    {
        return $this->container->get($name);
    }

    protected function getController(string $model)
    {
        return self::AVAILABLE_MODELS[$model];
    }


    public function get_method(Request $request, $response, $args)
    {
        $this->logger->debug(__METHOD__);
        $controller_class = $this->getController($args['model']);

        if (empty($controller_class)) {
            $controller_class = self::AVAILABLE_MODELS['Default'];
        }
        /** @var ControllerInterface $controller_class */
        $controller_class = new $controller_class($this->container);

        $method = $args['method'];
        $method = str_replace('_', '', lcfirst(ucwords($method)));

        if (is_callable(array($controller_class, $method))) {
            return $controller_class->$method($request, $response, $args);
        }

        return $controller_class->listRecords($request, $response, $args);
    }


}