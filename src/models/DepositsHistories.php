<?php

namespace tstech\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DepositsHistories
 * @package tstech\models
 * @property int $deposit_id
 * @property float $incoming_balance
 * @property float $outgoing_balance
 * @property float $amount
 * @property string $type
 */
class DepositsHistories extends Model
{
    const TYPE_INCREASE = 'accrual'; //увеличение баланса, начисление
    const TYPE_DECREASE = 'debit'; //уменьшение баланса, списание
    protected $id; //primary key
    protected $deposit_id; //link to deposits, foreign key, not null
    protected $incoming_balance; //входящий остаток
    protected $outgoing_balance; //исходящий остаток
    /**
     * @desc сумма списания или начисления, сделать валидацию по формуле
     * $outcoming_balance = $incoming_balance + $amount
     */

    protected $type; //enum accrual | debit
    protected $amount;

}