<?php

namespace tstech\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DepositsTypes
 * @package tstech\models
 * @property float $percent
 * @property float $lamount
 * @property float $uamount
 * @property float $lbound
 * @desc разные условия депозитов, правила по которым расчитываются начисления/списания,
 * например:
 * Баланс на счету: 1000 у.е. - до 10,000 у.е. Комисcия 6%
 */
class DepositsTypes extends Model
{
    protected $id; //primary key
    protected $name; //link to deposits, foreign key, not null
    protected $ubound; //баланс на счету до
    protected $lbound; //баланс на счету от
    protected $percent; //Комиссия %
    protected $lamount; //но не менее чем некоторая минимальная сумма
    protected $uamount; //но не менее чем некоторая минимальная сумма
    protected $status; //enum active | inactive

}