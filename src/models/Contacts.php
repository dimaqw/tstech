<?php

namespace tstech\models;

use Illuminate\Database\Eloquent\Model;
use tstech\models\Deposits;

class Contacts extends Model
{
    protected $id; //primary key
    protected $id_number; //unique
    protected $first_name;
    protected $last_name;
    protected $gender;
    protected $birhtday;
//    public $timestamps = false;


    public function deposits()
    {
        return $this->hasMany('tstech\models\Deposits', 'contact_id');
    }
}