<?php

namespace tstech\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Deposits
 * @package tstech\models
 * @property float $balance
 * @property int $id
 * @property float $percent
 */
class Deposits extends Model
{
    protected $id; //primary key
    protected $contact_id; //link to contact, foreign key
    protected $balance;
    protected $percent;
//    protected $created_at;
//    public $timestamps = false;

    public function histories()
    {
        return $this->hasMany('tstech\models\DepositsHistories', 'deposit_id');
    }

}