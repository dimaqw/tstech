<?php
// Routes

use tstech\controllers\DepositsController;
use tstech\controllers\ModelController;



$app->get('/info', function ($request, $response, $args) {

    return phpinfo();
})->setName('info');

$app->group('/api', function () {
    $this->map(['GET', 'POST'],'/{model}/{method}', ModelController::class . ':get_method')->setName('get_method');
});