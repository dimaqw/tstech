<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'tstech',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',         
            'database' => 'tstech',
            'username' => 'root',
            'password' => '123',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
//            uncomment for security mysql connection 
//            'options'   => array(
//                PDO::MYSQL_ATTR_SSL_KEY    => '/etc/app_certs/client-key.pem',
//                PDO::MYSQL_ATTR_SSL_CERT    => '/etc/app_certs/client-cert.pem',
//                PDO::MYSQL_ATTR_SSL_CA    => '/etc/app_certs/ca.pem',
//            ),
        ],
    ],
];
