create table tstech.contacts
(
  id int auto_increment
    primary key,
  id_number varchar(10) null,
  first_name varchar(255) null,
  last_name varchar(255) null,
  gender enum('male', 'female') null,
  birhtday date null,
  updated_at datetime null,
  created_at datetime null,
  constraint contacts_id_uindex
  unique (id),
  constraint contacts_id_number_uindex
  unique (id_number)
)
;

create table tstech.deposits
(
  id int auto_increment
    primary key,
  contact_id int not null,
  balance float(20,2) not null,
  updated_at datetime null,
  created_at datetime null,
  percent float(4,2) default '5.00' not null,
  constraint deposites_id_uindex
  unique (id)
)
;

create table tstech.deposits_histories
(
  id int auto_increment
    primary key,
  type enum('accrual', 'debit') null,
  incoming_balance float(20,2) null,
  outgoing_balance float(20,2) null,
  amount float(20,2) null,
  updated_at datetime null,
  created_at datetime null,
  deposit_id int not null,
  constraint deposits_history_id_uindex
  unique (id),
  constraint deposits_id
  foreign key (deposit_id) references tstech.deposits (id)
)
;

create index deposits_id
  on deposits_histories (deposit_id)
;

create table tstech.deposits_types
(
  id int auto_increment
    primary key,
  name varchar(255) null,
  lbound float(20,2) null,
  ubound float(20,2) null,
  percent float(4,2) null,
  lamount float(20,2) null,
  uamount float(20,2) null,
  status enum('active', 'inactive') null,
  updated_at datetime null,
  created_at datetime null,
  constraint deposits_type_id_uindex
  unique (id)
)
;

